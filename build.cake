#tool nuget:?package=NUnit.ConsoleRunner&version=3.11.1
#tool "nuget:?package=OpenCover&version=4.7.922"
#tool "nuget:?package=ReportGenerator&version=4.5.2"
#addin nuget:?package=Cake.ExtendedNuGet&version=2.1.1
#addin nuget:?package=Cake.FileHelpers&version=3.2.1

var target = Argument ("target", "Default");
var configuration = Argument ("configuration", "Release");

var solution = "StudentService.sln";
var releaseNotesPath = "./RELEASE_NOTES.md";
var binaries = "./Sources/StudentService.Instance/bin";
var objects = "./Sources/StudentService.Instance/obj";
var packages = "./artifacts/packages";
var artifacts = "./artifacts";

// CodeCoverage settings
var workdir = MakeAbsolute(Directory("."));
var coverageDirectory = $"{workdir}/Tests/CodeCoverageReport/";
var coverageFileName = $"coverage.xml";
var coverageFilePath = coverageDirectory + coverageFileName;
var codeCoverageReportType = "HtmlInline_AzurePipelines";

string version;

Task ("Clean")
    .Does (() => {
        CleanDirectories (new [] { artifacts, binaries, coverageDirectory });
        Information ($"Clean complete.");
    });

Task ("Restore")
    .Does (() => {
        var restoreSettings = new DotNetCoreRestoreSettings {
        PackagesDirectory = "./packages"
        };

        DotNetCoreRestore (restoreSettings);
        Information ($"Restore complete.");
    });

Task ("Version")
    .Does (() => {
        version = FileReadLines (releaseNotesPath).FirstOrDefault();
        Information ($"Estimated version is '{version}'.");

        var file = "./Sources/StudentService.Instance/AssemlyInfo.cs";
        CreateAssemblyInfo(file, new AssemblyInfoSettings {
            Title = "StudentService",
            Version = version,
            FileVersion = version,
            InformationalVersion = version,
            Copyright = $"Copyright (c) Aleksey Balandin 2019 - {DateTime.Now.Year}"
        });
    });

Task ("Build")
    .IsDependentOn ("Clean")
    .IsDependentOn ("Restore")
    .IsDependentOn ("Version")
    .Does (() => {
        DotNetCoreBuild (solution, new DotNetCoreBuildSettings {
            Configuration = configuration,
                NoRestore = true
        });
        Information ($"Build complete.");
    });

Task ("Publish")
    .IsDependentOn ("Build")
    .IsDependentOn ("Version")
    .Does (() => {
        var settings = new DotNetCorePublishSettings
        {
            Framework = "netcoreapp2.2",
            Configuration = "Release",
            OutputDirectory = "./artifacts/publish",
            NoBuild = true,
            NoRestore = true,
            Runtime = "win-x64"
        };

        DotNetCorePublish("./Sources/StudentService.Instance/StudentService.Instance.csproj", settings);
        Information ($"Publish complete.");
    });

Task ("Tests")
    .IsDependentOn ("Publish")
    .Does (() => {
        var testProjects = GetFiles("./Tests/**/*.csproj");
        Information ($"Found '{testProjects.Count}' test projects.");

        var settings = new DotNetCoreTestSettings
        {
            NoBuild = true,
            NoRestore = true,
            Verbosity = DotNetCoreVerbosity.Normal,
            Configuration = "Release"
        };

        foreach(var testProject in testProjects)
        {
            Information ($"Running test on '{testProject.FullPath}' project.");
            OpenCover(
                c => c.DotNetCoreTest(testProject.FullPath, settings),
                coverageFilePath,
                new OpenCoverSettings
                {
                    MergeOutput = true,
                    OldStyle = true
                }
            );            
        }

        Information ($"Tests complete.");
    });

Task("CodeCoverageReport")
    .IsDependentOn("Tests")
    .Does(() => 
            {
                var reportSettings = new ReportGeneratorSettings
                {
                    ArgumentCustomization = args => args.Append($"-reportTypes:{codeCoverageReportType}")
                };
                ReportGenerator(coverageFilePath, coverageDirectory, reportSettings);
            }
        );

Task ("Default")
    .IsDependentOn ("CodeCoverageReport");

RunTarget (target);