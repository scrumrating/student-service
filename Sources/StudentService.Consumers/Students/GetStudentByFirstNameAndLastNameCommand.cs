﻿namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Команда поиска студента по имени и фамилии.
    /// </summary>
    public class GetStudentByFirstNameAndLastNameCommand
    {
        /// <summary>
        /// Получает или задает имя студента.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Получает или задает фамилию студента.
        /// </summary>
        public string LastName { get; set; }
    }
}