﻿using MassTransit;
using Microsoft.Extensions.Logging;
using StudentService.Core.Interfaces;
using System.Threading.Tasks;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Обработчик сообщения получения информации о студенте по Id.
    /// </summary>
    public class CreateOneStudentConsumer : IConsumer<CreateOneStudentCommand>
    {
        private readonly IStudentsService service;
        private readonly ILogger<CreateOneStudentConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CreateOneStudentConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public CreateOneStudentConsumer(IStudentsService service, ILogger<CreateOneStudentConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<CreateOneStudentCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения регистрации студента '{context.Message.Student.LastName} {context.Message.Student.FirstName}'");

            bool result = this.service.CreateOne(context.Message.Student);

            if (result)
            {
                await context.RespondAsync(new CreateOneStudentResponse { Result = "success" });
            }
            else
            {
                await context.RespondAsync(new CreateOneStudentResponse { Result = "conflict" });
            }
        }
    }
}