﻿using MassTransit;
using Microsoft.Extensions.Logging;
using StudentService.Core.Dtos;
using StudentService.Core.Interfaces;
using System.Threading.Tasks;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Обработчик сообщения получения информации о студенте по Id.
    /// </summary>
    public class GetStudentByIdConsumer : IConsumer<GetStudentByIdCommand>
    {
        private readonly IStudentsService service;
        private readonly ILogger<GetStudentByIdConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetStudentByIdConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetStudentByIdConsumer(IStudentsService service, ILogger<GetStudentByIdConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetStudentByIdCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения поиска студента с идентификатором '{context.Message.Id}'");

            StudentDto student = this.service.Get(context.Message.Id);

            if (student != null)
            {
                await context.RespondAsync(new GetStudentByIdResponse { Student = student, Result = "success" });
            }
            else
            {
                await context.RespondAsync(new GetStudentByIdResponse { Result = "not-found" });
            }
        }
    }
}