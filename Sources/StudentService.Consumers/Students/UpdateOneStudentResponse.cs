﻿namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Ответ на команду обновления данных студента.
    /// </summary>
    public class UpdateOneStudentResponse
    {
        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}