﻿namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Ответ на команду регистрации студента.
    /// </summary>
    public class CreateOneStudentResponse
    {
        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}