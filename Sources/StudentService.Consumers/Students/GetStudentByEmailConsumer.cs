﻿using MassTransit;
using Microsoft.Extensions.Logging;
using StudentService.Core.Dtos;
using StudentService.Core.Interfaces;
using System.Threading.Tasks;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Обработчик сообщения получения информации о студенте по адресу электронной почты.
    /// </summary>
    public class GetStudentByEmailConsumer : IConsumer<GetStudentByEmailCommand>
    {
        private readonly IStudentsService service;
        private readonly ILogger<GetStudentByEmailConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetStudentByEmailConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetStudentByEmailConsumer(IStudentsService service, ILogger<GetStudentByEmailConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetStudentByEmailCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения поиска студента с адресом электронной почты '{context.Message.Email}'");

            StudentDto student = this.service.Get(context.Message.Email);

            if (student != null)
            {
                await context.RespondAsync(new GetStudentByEmailResponse { Student = student, Result = "success" });
            }
            else
            {
                await context.RespondAsync(new GetStudentByEmailResponse { Result = "not-found" });
            }
        }
    }
}