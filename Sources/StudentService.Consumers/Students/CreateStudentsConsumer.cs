﻿using MassTransit;
using Microsoft.Extensions.Logging;
using StudentService.Core.Interfaces;
using System.Threading.Tasks;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Обработчик сообщения получения информации о студенте по Id.
    /// </summary>
    public class CreateStudentsConsumer : IConsumer<CreateStudentsCommand>
    {
        private readonly IStudentsService service;
        private readonly ILogger<CreateStudentsConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CreateStudentsConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public CreateStudentsConsumer(IStudentsService service, ILogger<CreateStudentsConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<CreateStudentsCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения регистрации студентов в количестве '{context.Message.RegisteredStudents.Count}'.");
            this.service.CreateRange(context.Message.RegisteredStudents);
            await context.RespondAsync(new CreateStudentsResponse { Result = "success" });
        }
    }
}