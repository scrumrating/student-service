﻿using System.Collections.Generic;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Команда для поиска студентов по списку id.
    /// </summary>
    public class GetStudentsByIdRangeCommand
    {
        /// <summary>
        /// Получает или задает список id студентов для поиска.
        /// </summary>
        public List<int> Range { get; set; }
    }
}