﻿using StudentService.Core.Dtos;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Команда для регистрации студента.
    /// </summary>
    public class CreateOneStudentCommand
    {
        /// <summary>
        /// Получает или задает данные студента.
        /// </summary>
        public StudentDto Student { get; set; }
    }
}