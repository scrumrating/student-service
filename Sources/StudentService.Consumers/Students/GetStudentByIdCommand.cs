﻿namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Команда поиска студента по Id.
    /// </summary>
    public class GetStudentByIdCommand
    {
        /// <summary>
        /// Получает или задает Id студента.
        /// </summary>
        public int Id { get; set; }
    }
}