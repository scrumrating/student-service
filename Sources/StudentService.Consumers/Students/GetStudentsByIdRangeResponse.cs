﻿using StudentService.Core.Dtos;
using System.Collections.Generic;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Ответ на команду поиска поиска студентов по списку id.
    /// </summary>
    public class GetStudentsByIdRangeResponse
    {
        /// <summary>
        /// Получает или задает список студентов.
        /// </summary>
        public List<StudentDto> Students { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}