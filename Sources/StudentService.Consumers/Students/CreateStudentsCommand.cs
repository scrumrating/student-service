﻿using StudentService.Core.Dtos;
using System.Collections.Generic;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Команда для регистрации студентов.
    /// </summary>
    public class CreateStudentsCommand
    {
        /// <summary>
        /// Получает или задает данные студента.
        /// </summary>
        public List<StudentDto> RegisteredStudents { get; set; }
    }
}