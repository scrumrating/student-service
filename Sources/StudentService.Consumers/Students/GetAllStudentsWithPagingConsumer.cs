﻿using MassTransit;
using Microsoft.Extensions.Logging;
using StudentService.Core.Dtos;
using StudentService.Core.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Обработчик сообщения получения информации о всех студентах.
    /// </summary>
    public class GetAllStudentsWithPagingConsumer : IConsumer<GetAllStudentsWithPagingCommand>
    {
        private readonly IStudentsService service;
        private readonly ILogger<GetAllStudentsWithPagingConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetAllStudentsWithPagingConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetAllStudentsWithPagingConsumer(IStudentsService service, ILogger<GetAllStudentsWithPagingConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetAllStudentsWithPagingCommand> context)
        {
            this.logger.LogInformation("Выполняется обработка сообщения поиска всех студентов.");

            List<StudentDto> students = this.service.GetAll(context.Message.PageSize, context.Message.PageNumber);

            if (students != null)
            {
                await context.RespondAsync(new GetAllStudentsWithPagingResponse { Students = students, Result = "success" });
            }
            else
            {
                await context.RespondAsync(new GetAllStudentsWithPagingResponse { Result = "not-found" });
            }
        }
    }
}