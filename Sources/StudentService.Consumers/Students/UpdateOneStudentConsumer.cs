﻿using MassTransit;
using Microsoft.Extensions.Logging;
using StudentService.Core.Entities;
using StudentService.Core.Interfaces;
using System.Threading.Tasks;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Обработчик сообщения получения информации о студенте по Id.
    /// </summary>
    public class UpdateOneStudentConsumer : IConsumer<UpdateOneStudentCommand>
    {
        private readonly IStudentsService service;
        private readonly ILogger<UpdateOneStudentConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="UpdateOneStudentConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public UpdateOneStudentConsumer(IStudentsService service, ILogger<UpdateOneStudentConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<UpdateOneStudentCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения обновления данных студента '{context.Message.Student.LastName} {context.Message.Student.FirstName}'.");

            this.service.UpdateOne(context.Message.Student);
            await context.RespondAsync(new UpdateOneStudentResponse { Result = "success" });
        }
    }
}