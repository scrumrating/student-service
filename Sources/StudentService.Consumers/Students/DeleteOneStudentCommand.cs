﻿namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Команда для удаления данных студента.
    /// </summary>
    public class DeleteOneStudentCommand
    {
        /// <summary>
        /// Получает или задает идентификатор студента.
        /// </summary>
        public int Id { get; set; }
    }
}