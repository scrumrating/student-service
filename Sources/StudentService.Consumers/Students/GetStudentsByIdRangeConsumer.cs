﻿using MassTransit;
using Microsoft.Extensions.Logging;
using StudentService.Core.Dtos;
using StudentService.Core.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Обработчик сообщения получения информации о студентах по списку id.
    /// </summary>
    public class GetStudentsByIdRangeConsumer : IConsumer<GetStudentsByIdRangeCommand>
    {
        private readonly IStudentsService service;
        private readonly ILogger<GetStudentsByIdRangeConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetStudentsByIdRangeConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetStudentsByIdRangeConsumer(IStudentsService service, ILogger<GetStudentsByIdRangeConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetStudentsByIdRangeCommand> context)
        {
            this.logger.LogInformation("Выполняется обработка сообщения поиска студентов по списку идентификаторов.");

            List<StudentDto> students = this.service.Get(context.Message.Range);

            if (students != null)
            {
                await context.RespondAsync(new GetStudentsByIdRangeResponse { Students = students, Result = "success" });
            }
            else
            {
                await context.RespondAsync(new GetStudentsByIdRangeResponse { Result = "not-found" });
            }
        }
    }
}