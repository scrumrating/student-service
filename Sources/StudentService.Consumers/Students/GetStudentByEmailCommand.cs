﻿namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Команда поиска студента по адресу электронной почты.
    /// </summary>
    public class GetStudentByEmailCommand
    {
        /// <summary>
        /// Получает или задает адрес электронной почты.
        /// </summary>
        public string Email { get; set; }
    }
}