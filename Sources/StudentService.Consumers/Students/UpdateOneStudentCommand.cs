﻿using StudentService.Core.Dtos;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Команда для обновления данных студента.
    /// </summary>
    public class UpdateOneStudentCommand
    {
        /// <summary>
        /// Получает или задает данные студента.
        /// </summary>
        public StudentDto Student { get; set; }
    }
}