﻿using StudentService.Core.Dtos;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Ответ на команду получения студента по имени и фамилии.
    /// </summary>
    public class GetStudentByFirstNameAndLastNameResponse
    {
        /// <summary>
        /// Получает или задает студента.
        /// </summary>
        public StudentDto Student { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}