﻿using StudentService.Core.Dtos;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Ответ на команду поиска информации о студенте по адресу электронной почты.
    /// </summary>
    public class GetStudentByEmailResponse
    {
        /// <summary>
        /// Получает или задает студента.
        /// </summary>
        public StudentDto Student { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}