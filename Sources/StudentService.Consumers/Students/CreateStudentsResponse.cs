﻿namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Ответ на команду регистрации студентов.
    /// </summary>
    public class CreateStudentsResponse
    {
        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}