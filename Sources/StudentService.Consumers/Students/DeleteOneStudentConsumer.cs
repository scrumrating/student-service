﻿using MassTransit;
using Microsoft.Extensions.Logging;
using StudentService.Core.Entities;
using StudentService.Core.Interfaces;
using System.Threading.Tasks;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Обработчик сообщения получения информации о студенте по Id.
    /// </summary>
    public class DeleteOneStudentConsumer : IConsumer<DeleteOneStudentCommand>
    {
        private readonly IStudentsService service;
        private readonly ILogger<DeleteOneStudentConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="DeleteOneStudentConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public DeleteOneStudentConsumer(IStudentsService service, ILogger<DeleteOneStudentConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<DeleteOneStudentCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения удаления данных студента с id '{context.Message.Id}'");

            this.service.DeleteOne(context.Message.Id);
            await context.RespondAsync(new DeleteOneStudentResponse { Result = "success" });
        }
    }
}