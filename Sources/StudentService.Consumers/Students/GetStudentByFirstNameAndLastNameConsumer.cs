﻿using MassTransit;
using Microsoft.Extensions.Logging;
using StudentService.Core.Dtos;
using StudentService.Core.Entities;
using StudentService.Core.Interfaces;
using System.Threading.Tasks;

namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Обработчик сообщения получения информации о студенте по имени и фамилии.
    /// </summary>
    public class GetStudentByFirstNameAndLastNameConsumer : IConsumer<GetStudentByFirstNameAndLastNameCommand>
    {
        private readonly IStudentsService service;
        private readonly ILogger<GetStudentByFirstNameAndLastNameConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetStudentByFirstNameAndLastNameConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetStudentByFirstNameAndLastNameConsumer(IStudentsService service, ILogger<GetStudentByFirstNameAndLastNameConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetStudentByFirstNameAndLastNameCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения поиска студента с именем '{context.Message.FirstName}' и фамилией '{context.Message.LastName}'");

            StudentDto student = this.service.Get(context.Message.FirstName, context.Message.LastName);

            if (student != null)
            {
                await context.RespondAsync(new GetStudentByFirstNameAndLastNameResponse { Student = student, Result = "success" });
            }
            else
            {
                await context.RespondAsync(new GetStudentByFirstNameAndLastNameResponse { Result = "not-found" });
            }
        }
    }
}