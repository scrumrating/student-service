﻿namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Команда для поиска всех студентов.
    /// </summary>
    public class GetAllStudentsWithPagingCommand
    {
        /// <summary>
        /// Получает или задает размер страницы.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Получает или задает номер страницы.
        /// </summary>
        public int PageNumber { get; set; }
    }
}