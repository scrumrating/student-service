﻿namespace StudentService.Consumers.Students
{
    /// <summary>
    /// Ответ на команду удаления данных студента.
    /// </summary>
    public class DeleteOneStudentResponse
    {
        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}