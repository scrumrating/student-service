﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using StudentService.Consumers.Students;
using StudentService.Core.Dtos;
using StudentService.Core.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace StudentService.WebApi.Controllers
{
    /// <summary>
    /// Контроллер Students.
    /// </summary>
    [Route("api/student")]
    public class StudentsController : Controller
    {
        private readonly IRequestClient<GetStudentByFirstNameAndLastNameCommand> getStudentByFirstNameAndLastNameClient;
        private readonly IRequestClient<GetStudentByEmailCommand> getStudentByEmailClient;
        private readonly IRequestClient<GetStudentsByIdRangeCommand> getStudentsByIdRangeClient;
        private readonly IRequestClient<GetAllStudentsWithPagingCommand> getAllStudentsWithPagingClient;
        private readonly IRequestClient<GetAllStudentsCommand> getAllStudentClient;
        private readonly IRequestClient<GetStudentByIdCommand> getStudentsByIdClient;
        private readonly IRequestClient<CreateOneStudentCommand> createOneStudentClient;
        private readonly IRequestClient<CreateStudentsCommand> createStudentsClient;
        private readonly IRequestClient<UpdateOneStudentCommand> updateOneStudentClient;
        private readonly IRequestClient<DeleteOneStudentCommand> deleteOneStudentClient;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="StudentsController"/>.
        /// </summary>
        /// <param name="getStudentByFirstNameAndLastNameClient">Клиент получения информации о студенте по фамилии и имени.</param>
        /// <param name="getStudentByEmailClient">Клиент для получения информации о студенте по адресу электронной почты.</param>
        /// <param name="getAllStudentsWithPagingClient">Клиент для постраничного получения информации о всех студентах.</param>
        /// <param name="getAllStudentClient">Клиент для получения информации о всех студентах.</param>
        /// <param name="getStudentsByIdClient">Клиент для получения информации о студенте по его идентификатору.</param>
        /// <param name="getStudentsByIdRangeClient">Клиент для получения информации о студентах по списку идентификаторов.</param>
        /// <param name="createOneStudentClient">Клиент для регистрации студента.</param>
        /// <param name="createStudentsClient">Клиент для регистрации студентов.</param>
        /// <param name="updateOneStudentClient">Клиент для обновления студентов.</param>
        /// <param name="deleteOneStudentClient">Клиент для удаления данных студента.</param>
        public StudentsController(
            IRequestClient<GetStudentByFirstNameAndLastNameCommand> getStudentByFirstNameAndLastNameClient,
            IRequestClient<GetStudentByEmailCommand> getStudentByEmailClient,
            IRequestClient<GetAllStudentsWithPagingCommand> getAllStudentsWithPagingClient,
            IRequestClient<GetAllStudentsCommand> getAllStudentClient,
            IRequestClient<GetStudentByIdCommand> getStudentsByIdClient,
            IRequestClient<GetStudentsByIdRangeCommand> getStudentsByIdRangeClient,
            IRequestClient<CreateOneStudentCommand> createOneStudentClient,
            IRequestClient<CreateStudentsCommand> createStudentsClient,
            IRequestClient<UpdateOneStudentCommand> updateOneStudentClient,
            IRequestClient<DeleteOneStudentCommand> deleteOneStudentClient)
        {
            this.getStudentByFirstNameAndLastNameClient = getStudentByFirstNameAndLastNameClient;
            this.getStudentByEmailClient = getStudentByEmailClient;
            this.getAllStudentsWithPagingClient = getAllStudentsWithPagingClient;
            this.getAllStudentClient = getAllStudentClient;
            this.getStudentsByIdClient = getStudentsByIdClient;
            this.getStudentsByIdRangeClient = getStudentsByIdRangeClient;
            this.createOneStudentClient = createOneStudentClient;
            this.createStudentsClient = createStudentsClient;
            this.updateOneStudentClient = updateOneStudentClient;
            this.deleteOneStudentClient = deleteOneStudentClient;
        }

        /// <summary>
        /// Метод для получения доступным методов.
        /// </summary>
        /// <returns>Список доступных методов.</returns>
        [HttpOptions]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult Options()
        {
            this.Response.Headers.Add("Allow", "OPTIONS, GET, POST, PATCH, DELETE");
            return this.Ok();
        }

        /// <summary>
        /// Получить информацию о студенте по имени и фамилии.
        /// </summary>
        /// <param name="firstName">Имя.</param>
        /// <param name="lastName">Фамилия.</param>
        /// <returns>Данные о найденном студенте.</returns>
        /// <response code="200">Студент найден.</response>
        /// <response code="404">Студент не найден.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [HttpGet]
        [Route("One/ByFullName")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<IActionResult> Get([FromQuery(Name = "firstname")] string firstName, [FromQuery(Name = "lastname")] string lastName)
        {
            try
            {
                var command = new GetStudentByFirstNameAndLastNameCommand() { FirstName = firstName, LastName = lastName };
                var response = await this.getStudentByFirstNameAndLastNameClient.GetResponse<GetStudentByFirstNameAndLastNameResponse>(command);

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.Student);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить информации о студенте по адресу электронной почты.
        /// </summary>
        /// <param name="email">Адрес электронной почты.</param>
        /// <returns>Данные о найденном студенте.</returns>
        /// <response code="200">Студент найден.</response>
        /// <response code="404">Студент не найден.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [HttpGet]
        [Route("One/ByEmail")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<IActionResult> Get([FromQuery(Name = "email")] string email)
        {
            try
            {
                var command = new GetStudentByEmailCommand() { Email = email };
                var response = await this.getStudentByEmailClient.GetResponse<GetStudentByEmailResponse>(command);

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.Student);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить информацию о студенте по идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <returns>Данные искомого студента.</returns>
        /// <response code="200">Студент найден.</response>
        /// <response code="404">Студент не найден.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [HttpGet]
        [Route("One/ById")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<IActionResult> Get([FromQuery(Name = "id")] int id)
        {
            try
            {
                Response<GetStudentByIdResponse> response = await this.getStudentsByIdClient.GetResponse<GetStudentByIdResponse>(new GetStudentByIdCommand { Id = id });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok(response.Message.Student);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить информацию о всех студентах постранично.
        /// </summary>
        /// <param name="pageSize">Количество строк для получения.</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <returns>Список студентов.</returns>
        /// <response code="200">Список студентов получен.</response>
        /// <response code="404">Не найдено ни одного студента.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [HttpGet]
        [Route("All/Paging")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<IActionResult> GetAll([FromQuery] int pageSize, [FromQuery] int pageNumber)
        {
            try
            {
                var command = new GetAllStudentsWithPagingCommand()
                {
                    PageSize = pageSize,
                    PageNumber = pageNumber,
                };

                var response = await this.getAllStudentsWithPagingClient.GetResponse<GetAllStudentsWithPagingResponse>(command);

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.Students);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить информацию о всех студентах.
        /// </summary>
        /// <returns>Список студентов.</returns>
        /// <response code="200">Список студентов получен.</response>
        /// <response code="404">Не найдено ни одного студента.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [HttpGet]
        [Route("All")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var response = await this.getAllStudentClient.GetResponse<GetAllStudentsResponse>(new GetAllStudentsCommand());

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.Students);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить информацию о студентах по списку идентификаторов.
        /// </summary>
        /// <returns>Список студентов.</returns>
        /// <response code="200">Список студентов получен.</response>
        /// <response code="404">Не найдено ни одного студента.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [HttpGet]
        [Route("Range")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<IActionResult> GetRange([FromBody] List<int> range)
        {
            try
            {
                var command = new GetStudentsByIdRangeCommand { Range = range };
                var response = await this.getStudentsByIdRangeClient.GetResponse<GetStudentsByIdRangeResponse>(command);

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.Students);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Зарегистрировать студента.
        /// </summary>
        /// <param name="student">Данные регистируемого студента.</param>
        /// <response code="200">Студент зарегистрирован.</response>
        /// <response code="409">Такой студент уже зарегистрирован.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [HttpPost]
        [Route("Create")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.Conflict)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<IActionResult> Post([FromBody] StudentDto student)
        {
            try
            {
                Response<CreateOneStudentResponse> response = await this.createOneStudentClient.GetResponse<CreateOneStudentResponse>(new CreateOneStudentCommand { Student = student });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok();
                    case "conflict":
                        return new StatusCodeResult((int)HttpStatusCode.Conflict);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Зарегистрировать студентов.
        /// </summary>
        /// <param name="students">Список студентов для регистрации.</param>
        /// <response code="200">Студенты зарегистрированы.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [HttpPost]
        [Route("CreateRange")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<IActionResult> Post([FromBody] List<StudentDto> students)
        {
            try
            {
                var response = await this.createStudentsClient.GetResponse<CreateStudentsResponse>(new CreateStudentsCommand { RegisteredStudents = students });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok();
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Обновить данные студента.
        /// </summary>
        /// <param name="student">Данные студента.</param>
        /// <response code="200">Данные студента обновлены.</response>
        /// <response code="204">Студент не найден.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [HttpPatch]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<IActionResult> Patch([FromBody] StudentDto student)
        {
            try
            {
                var response = await this.updateOneStudentClient.GetResponse<UpdateOneStudentResponse>(new UpdateOneStudentCommand { Student = student });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok();
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.NoContent);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Удалить данные студента.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <response code="200">Данные студента удалены.</response>
        /// <response code="502">Произошла внутренняя ошибка.</response>
        /// <response code="504">Timeout.</response>
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        public async Task<IActionResult> Delete([FromQuery] int id)
        {
            try
            {
                var response = await this.deleteOneStudentClient.GetResponse<DeleteOneStudentResponse>(new DeleteOneStudentCommand { Id = id });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok();
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }
    }
}