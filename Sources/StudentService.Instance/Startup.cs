﻿using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Serilog;
using StudentService.Consumers.Healthchecks;
using StudentService.Consumers.Students;
using StudentService.Core.Interfaces;
using StudentService.Core.Services;
using StudentService.Data.Contexts;
using StudentService.Data.Repositories;
using StudentService.WebApi;
using System;
using System.IO;

namespace StudentService.Instance
{
    /// <summary>
    /// Инициализация приложения.
    /// </summary>
    public class Startup
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Startup"/>.
        /// </summary>
        public Startup(IHostingEnvironment environment)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            this.configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(this.configuration)
                    .CreateLogger();
        }

        /// <summary>s
        /// Конфигурация приложения.
        /// </summary>
        /// <param name="application">Приложение.</param>
        public void Configure(IApplicationBuilder application)
        {
            this.ConfigureDatabase(application);
            application.UseCors("AllowAll");
            application.UseMvc();
            application.UseSwagger();
            var swaggerCongiguration = this.configuration.GetSection("Swagger").Get<SwaggerCongiguration>();
            application.UseSwaggerUI(c => { c.SwaggerEndpoint(swaggerCongiguration.Uri, swaggerCongiguration.Name); });
        }

        /// <summary>
        /// Конфигурация сервисов.
        /// </summary>
        /// <param name="services">Сервисы.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            Log.Information("Начинается регистрация политик CORS.");

            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            Log.Information("Регистрация политик CORS успешно завершена.");
            Log.Information("Начинается регистрация сервисов.");

            Log.Information("Начинается регистрация Swagger генератора.");
            SwaggerCongiguration swaggerCongiguration = this.configuration.GetSection("Swagger").Get<SwaggerCongiguration>();

            var apiInfo = new OpenApiInfo
            {
                Title = swaggerCongiguration.Title,
                Version = swaggerCongiguration.Version,
                Description = swaggerCongiguration.Description,
            };
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", apiInfo);
                var xmlFile = "StudentService.WebApi.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            Log.Information("Регистрация Swagger генератора завершена.");
            Log.Information("Регистрация сервиса Student");

            services.AddDbContext<StudentsDbContext>(option => option.UseNpgsql(this.configuration.GetConnectionString("StudentsService")));

            services.AddScoped<IStudentsService, StudentsService>();
            services.AddScoped<IStudentsRepository, StudentsRepository>();
            services.AddSingleton<GetStudentByFirstNameAndLastNameConsumer>();
            services.AddSingleton<GetStudentByEmailConsumer>();
            services.AddSingleton<GetAllStudentsWithPagingConsumer>();
            services.AddSingleton<GetAllStudentsConsumer>();
            services.AddSingleton<GetStudentByIdConsumer>();
            services.AddSingleton<GetStudentsByIdRangeConsumer>();
            services.AddSingleton<CreateOneStudentConsumer>();
            services.AddSingleton<CreateStudentsConsumer>();
            services.AddSingleton<UpdateOneStudentConsumer>();
            services.AddSingleton<DeleteOneStudentConsumer>();

            Log.Information("Сервис Student зарегистрирован.");
            Log.Information("Регистрация сервисов завершена.");

            Log.Information("Начинается регистрация шины.");

            // Регистрация потребителей сообщений
            services.AddMassTransit(x =>
            {
                x.AddConsumer<HealthcheckConsumer>();
                x.AddConsumer<GetStudentByFirstNameAndLastNameConsumer>();
                x.AddConsumer<GetStudentByEmailConsumer>();
                x.AddConsumer<GetAllStudentsWithPagingConsumer>();
                x.AddConsumer<GetAllStudentsConsumer>();
                x.AddConsumer<GetStudentByIdConsumer>();
                x.AddConsumer<GetStudentsByIdRangeConsumer>();
                x.AddConsumer<CreateOneStudentConsumer>();
                x.AddConsumer<CreateStudentsConsumer>();
                x.AddConsumer<UpdateOneStudentConsumer>();
                x.AddConsumer<DeleteOneStudentConsumer>();
            });

            // Регистрация шины.
            // Подробнее про регистрацию шины можно почитать здесь: https://masstransit-project.com/usage/
            services.AddSingleton(serviceProvider => Bus.Factory.CreateUsingRabbitMq(configure =>
            {
                BusConfiguration busConfiguration = this.configuration.GetSection("Bus").Get<BusConfiguration>();

                // Конфигурация подключения к шине, включающая в себя указание адреса и учетных данных.
                configure.Host(new Uri(busConfiguration.ConnectionString), host =>
                {
                    host.Username(busConfiguration.Username);
                    host.Password(busConfiguration.Password);

                    // Подтверждение получения гарантирует доставку сообщений за счет ухудшения производительности.
                    host.PublisherConfirmation = busConfiguration.PublisherConfirmation;
                });

                // Добавление Serilog для журналирования внутренностей MassTransit.
                configure.UseSerilog();

                // Регистрация очередей и их связи с потребителями сообщений.
                // В качестве метки сообщения используется полное имя класса сообщения, которое потребляет потребитель.
                configure.ReceiveEndpoint(typeof(HealthcheckCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<HealthcheckConsumer>(serviceProvider);
                    EndpointConvention.Map<HealthcheckCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetStudentByFirstNameAndLastNameCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetStudentByFirstNameAndLastNameConsumer>(serviceProvider);
                    EndpointConvention.Map<GetStudentByFirstNameAndLastNameCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetStudentByEmailCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetStudentByEmailConsumer>(serviceProvider);
                    EndpointConvention.Map<GetStudentByEmailCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetAllStudentsWithPagingCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetAllStudentsWithPagingConsumer>(serviceProvider);
                    EndpointConvention.Map<GetAllStudentsWithPagingCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetAllStudentsCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetAllStudentsConsumer>(serviceProvider);
                    EndpointConvention.Map<GetAllStudentsCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetStudentByIdCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetStudentByIdConsumer>(serviceProvider);
                    EndpointConvention.Map<GetStudentByIdCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(GetStudentsByIdRangeCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<GetStudentsByIdRangeConsumer>(serviceProvider);
                    EndpointConvention.Map<GetStudentsByIdRangeCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(CreateOneStudentCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<CreateOneStudentConsumer>(serviceProvider);
                    EndpointConvention.Map<CreateOneStudentCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(CreateStudentsCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<CreateStudentsConsumer>(serviceProvider);
                    EndpointConvention.Map<CreateStudentsCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(UpdateOneStudentCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<UpdateOneStudentConsumer>(serviceProvider);
                    EndpointConvention.Map<UpdateOneStudentCommand>(endpoint.InputAddress);
                });

                configure.ReceiveEndpoint(typeof(DeleteOneStudentCommand).FullName, endpoint =>
                {
                    endpoint.Consumer<DeleteOneStudentConsumer>(serviceProvider);
                    EndpointConvention.Map<DeleteOneStudentCommand>(endpoint.InputAddress);
                });
            }));

            // Регистрация сервисов MassTransit.
            services.AddSingleton<IPublishEndpoint>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<ISendEndpointProvider>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<IBus>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());

            // Регистрация клиентов для запроса данных от потребителей сообщений из api.
            // Каждый клиент зарегистрирован таким образом, что бы в рамках каждого запроса к api существовал свой клиент.
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<HealthcheckCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetStudentByFirstNameAndLastNameCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetStudentByEmailCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetAllStudentsWithPagingCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetAllStudentsCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetStudentByIdCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetStudentsByIdRangeCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<CreateOneStudentCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<CreateStudentsCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<UpdateOneStudentCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<DeleteOneStudentCommand>());

            // Регистрация сервиса шины MassTransit.
            services.AddSingleton<IHostedService, BusService>();
            Log.Information("Регистрация шины завершена.");

            Log.Information("Начинается регистрация сервисов MVC.");
            services.AddMvc();
            Log.Information("Регистрация сервисов MVC завершена.");
        }

        /// <summary>
        /// Обновляет или разворачивает базу данных сервиса.
        /// </summary>
        /// <param name="application">Приложение.</param>
        private void ConfigureDatabase(IApplicationBuilder application)
        {
            Log.Information("Начинается создание/обновление базы данных.");
            var context = application.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<StudentsDbContext>();
            context.Database.Migrate();
            Log.Information("Создание/обновление базы данных завершено.");
        }
    }
}