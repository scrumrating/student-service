﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace StudentService.Data.Contexts
{
    /// <summary>
    /// Фабрика для генераци контекста, необходимого для создания миграций.
    /// </summary>
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<StudentsDbContext>
    {
        /// <inheritdoc/>
        public StudentsDbContext CreateDbContext(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("appsettings.production.json", true, true)
                .Build();

            var builder = new DbContextOptionsBuilder<StudentsDbContext>();
            builder.UseNpgsql(configuration.GetConnectionString("StudentsService"));
            return new StudentsDbContext(builder.Options);
        }
    }
}