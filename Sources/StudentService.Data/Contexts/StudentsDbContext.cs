﻿using Microsoft.EntityFrameworkCore;
using StudentService.Core.Entities;

namespace StudentService.Data.Contexts
{
    /// <summary>
    /// Контекст для доступа к данным.
    /// </summary>
    public class StudentsDbContext : DbContext
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="StudentsDbContext"/>.
        /// </summary>
        public StudentsDbContext(DbContextOptions<StudentsDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Получает или задает DbSet для работы с данными студентов.
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            var studentEntity = builder.Entity<Student>();
            studentEntity.HasKey(k => k.Id);

            studentEntity.Property(p => p.Id).ValueGeneratedOnAdd();
            studentEntity.Property(p => p.FirstName).IsRequired().HasMaxLength(100);
            studentEntity.Property(p => p.MiddleName).HasMaxLength(100);
            studentEntity.Property(p => p.LastName).IsRequired().HasMaxLength(100);
            studentEntity.Property(p => p.Email).IsRequired().HasMaxLength(100);
        }
    }
}