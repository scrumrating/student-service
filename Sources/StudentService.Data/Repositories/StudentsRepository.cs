﻿using StudentService.Core.Entities;
using StudentService.Core.Interfaces;
using StudentService.Data.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace StudentService.Data.Repositories
{
    /// <summary>
    /// Репозиторий для работы с данными студентов.
    /// </summary>
    public class StudentsRepository : IStudentsRepository
    {
        private readonly StudentsDbContext context;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="StudentsRepository"/>.
        /// </summary>
        public StudentsRepository(StudentsDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Найти студента по имени и фамилии.
        /// </summary>
        /// <param name="firstName">Имя.</param>
        /// <param name="lastName">Фамилия.</param>
        /// <returns>Экземпляр класса <see cref="Student"/>.</returns>
        public Student Find(string firstName, string lastName)
        {
            return this.context.Students.Where(st => st.FirstName.Equals(firstName.Trim(), StringComparison.InvariantCulture)
                                                     && st.LastName.Equals(lastName.Trim(), StringComparison.InvariantCulture))
                                                     .FirstOrDefault();
        }

        /// <summary>
        /// Найти студента по адресу электронной почты.
        /// </summary>
        /// <param name="email">Адрес электронной почты.</param>
        /// <returns>Экземпляр класса <see cref="Student"/>.</returns>
        public Student Find(string email)
        {
            return this.context.Students.Where(st => st.Email.Equals(email.Trim(), StringComparison.InvariantCulture)).FirstOrDefault();
        }

        /// <summary>
        /// Найти студента по Id.
        /// </summary>
        /// <param name="id">Идентификатор студента.</param>
        /// <returns>Экземпляр класса <see cref="Student"/>.</returns>
        public Student Find(int id)
        {
            return this.context.Students.FirstOrDefault(st => st.Id == id);
        }

        /// <summary>
        /// Найти всех студентов с id из списка.
        /// </summary>
        /// <param name="range">Список идентификаторов студентов.</param>
        /// <returns>Список студентов.</returns>
        public List<Student> Find(List<int> range)
        {
            return (from st in this.context.Students
                    join r in range on st.Id equals r
                    select st).ToList();
        }

        /// <summary>
        /// Найти всех студентов.
        /// </summary>
        /// <param name="pageSize">Количество строк для получения.</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <returns>Список студентов.</returns>
        public List<Student> FindAll(int pageSize, int pageNumber)
        {
            return this.context.Students
                               .Skip((pageNumber - 1) * pageSize)
                               .Take(pageSize)
                               .ToList();
        }

        /// <summary>
        /// Найти всех студентов.
        /// </summary>
        /// <returns>Список студентов.</returns>
        public List<Student> FindAll()
        {
            return this.context.Students.ToList();
        }

        /// <summary>
        /// Зарегистрировать студента.
        /// </summary>
        /// <param name="student">Данные студента.</param>
        /// <returns>Признак успешной регистрации студента.</returns>
        public bool Register(Student student)
        {
            bool isExist = this.context.Students.Any(s => s.Email == student.Email);
            bool hasError = false;

            if (isExist == false)
            {
                this.context.Students.Add(student);
                var count = this.context.SaveChanges();
                hasError = count > 0 ? true : false;
            }

            return hasError;
        }

        /// <summary>
        /// Зарегистрировать студентов.
        /// </summary>
        /// <param name="students">Список студентов для регистрации.</param>
        public void RegisterRange(List<Student> students)
        {
            var newStudents = students.Except(this.context.Students);
            this.context.Students.AddRange(newStudents);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Обновить данные студента.
        /// </summary>
        /// <param name="student">Данные студента.</param>
        public void Update(Student student)
        {
            this.context.Students.Update(student);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Удалить данные студента.
        /// </summary>
        /// <param name="id">Идентификатор студента.</param>
        public void Delete(int id)
        {
            Student student = this.context.Students.Find(id);
            this.context.Students.Remove(student);
            this.context.SaveChanges();
        }

    }
}