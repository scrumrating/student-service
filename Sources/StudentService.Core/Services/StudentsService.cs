﻿using Microsoft.Extensions.Logging;
using StudentService.Core.Dtos;
using StudentService.Core.Entities;
using StudentService.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudentService.Core.Services
{
    /// <summary>
    /// Сервисный объект для взаимодействия с данными студентов.
    /// </summary>
    public class StudentsService : IStudentsService
    {
        private readonly IStudentsRepository repository;
        private readonly ILogger<StudentsService> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="StudentsService"/>.
        /// </summary>
        /// <param name="repository">Хранилище данных о студентах.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public StudentsService(IStudentsRepository repository, ILogger<StudentsService> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        /// <summary>
        /// Найти студента по имени и фамилии.
        /// </summary>
        /// <param name="firstName">Имя.</param>
        /// <param name="lastName">Фамилия.</param>
        /// <returns>Экземпляр класса <see cref="Student"/>.</returns>
        public StudentDto Get(string firstName, string lastName)
        {
            this.logger.LogInformation($"Выполняется поиск студента с именем '{firstName}' и фамилией '{lastName}'");

            Student student = this.repository.Find(firstName, lastName);
            StudentDto result = null;

            if (student != null)
            {
                result = new StudentDto(student.Id, student.FirstName, student.MiddleName, student.LastName, student.Email);
            }

            return result;
        }

        /// <summary>
        /// Найти студента по адресу электронной почты.
        /// </summary>
        /// <param name="email">Адрес электронной почты.</param>
        /// <returns>Экземпляр класса <see cref="StudentDto"/>.</returns>
        public StudentDto Get(string email)
        {
            this.logger.LogInformation($"Выполняется поиск студента с адресом электронной почты '{email}'");

            Student student = this.repository.Find(email);
            StudentDto result = null;

            if (student != null)
            {
                result = new StudentDto(student.Id, student.FirstName, student.MiddleName, student.LastName, student.Email);
            }

            return result;
        }

        /// <summary>
        /// Найти студента по Id.
        /// </summary>
        /// <param name="id">Идентификатор студента.</param>
        /// <returns>Экземпляр класса <see cref="StudentDto"/>.</returns>
        public StudentDto Get(int id)
        {
            this.logger.LogInformation($"Выполняется поиск студента с id '{id}'");

            Student student = this.repository.Find(id);
            StudentDto result = null;

            if (student != null)
            {
                result = new StudentDto(student.Id, student.FirstName, student.MiddleName, student.LastName, student.Email);
            }

            return result;
        }

        /// <summary>
        /// Найти всех студентов с id из списка.
        /// </summary>
        /// <param name="range">Список идентификаторов студентов.</param>
        /// <returns>Список студентов.</returns>
        public List<StudentDto> Get(List<int> range)
        {
            this.logger.LogInformation($"Выполняется поиск студентов с id из списка.");
            var students = this.repository.Find(range);
            return students.Select(st => new StudentDto(st.Id, st.FirstName, st.MiddleName, st.LastName, st.Email)).ToList();
        }

        /// <summary>
        /// Найти всех студентов.
        /// </summary>
        /// <param name="pageSize">Количество строк для получения.</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <returns>Список студентов.</returns>
        public List<StudentDto> GetAll(int pageSize, int pageNumber)
        {
            this.logger.LogInformation($"Выполняется постраничный поиск всех студентов.");
            var students = this.repository.FindAll(pageSize, pageNumber);
            return students.Select(st => new StudentDto(st.Id, st.FirstName, st.MiddleName, st.LastName, st.Email)).ToList();
        }

        /// <summary>
        /// Найти всех студентов.
        /// </summary>
        /// <returns>Список студентов.</returns>
        public List<StudentDto> GetAll()
        {
            this.logger.LogInformation($"Выполняется поиск всех студентов.");
            var students = this.repository.FindAll();
            return students.Select(st => new StudentDto(st.Id, st.FirstName, st.MiddleName, st.LastName, st.Email)).ToList();
        }

        /// <summary>
        /// Зарегистрировать студента.
        /// </summary>
        /// <param name="student">Данные регистрируемого студента.</param>
        public bool CreateOne(StudentDto student)
        {
            this.logger.LogInformation($"Выполняется создание студента '{student.LastName} {student.FirstName}'");
            return this.repository.Register(new Student(student.Id, student.FirstName, student.MiddleName, student.LastName, student.Email));
        }

        /// <summary>
        /// Зарегистрировать студентов.
        /// </summary>
        /// <param name="students">Список студентов для регистрации.</param>
        public void CreateRange(List<StudentDto> students)
        {
            this.logger.LogInformation($"Выполняется регистрация студентов в количестве {students.Count}.");
            List<Student> newStudents = students.Select(st => new Student(st.Id, st.FirstName, st.MiddleName, st.LastName, st.Email)).ToList();
            this.repository.RegisterRange(newStudents);
        }

        /// <summary>
        /// Обновить данные студента.
        /// </summary>
        /// <param name="student">Данные студента.</param>
        public void UpdateOne(StudentDto student)
        {
            this.logger.LogInformation($"Выполняется обновление данных студента '{student.LastName} {student.FirstName}'.");
            this.repository.Update(new Student(student.Id, student.FirstName, student.MiddleName, student.LastName, student.Email));
        }

        /// <summary>
        /// Удалить данные студента.
        /// </summary>
        /// <param name="id">Идентификатор студента.</param>
        public void DeleteOne(int id)
        {
            this.logger.LogInformation($"Выполняется удаление данныз студента с Id '{id}'.");
            this.repository.Delete(id);
        }
    }
}