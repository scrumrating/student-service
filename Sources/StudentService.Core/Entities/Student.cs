﻿using System;

namespace StudentService.Core.Entities
{
    /// <summary>
    /// Сущность "Студент".
    /// </summary>
    public class Student
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Student"/>.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <param name="firstName">Имя.</param>
        /// <param name="middleName">Отчество.</param>
        /// <param name="lastName">Фамилия.</param>
        /// <param name="email">Адрес электронной почты.</param>
        public Student(int id, string firstName, string middleName, string lastName, string email)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.MiddleName = middleName;
            this.LastName = lastName;
            this.Email = email;
        }

        /// <summary>
        /// Получает или задает идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Получает или задает имя студента.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Получает или задает отчество студента.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Получает или задает фамилию студента.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Получает или задает адрес электронной почты.
        /// </summary>
        public string Email { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Id = {this.Id}, FirstName = {this.FirstName}, MiddleName = {this.MiddleName}, LastName = {this.LastName}, Email = {this.Email}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            bool isEqual = false;

            if (obj is Student)
            {
                var compareToObj = obj as Student;

                if (compareToObj.Email == this.Email)
                {
                    isEqual = true;
                }
            }

            return isEqual;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Email.GetHashCode(StringComparison.InvariantCulture);
        }
    }
}