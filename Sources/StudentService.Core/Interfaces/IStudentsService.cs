﻿using StudentService.Core.Dtos;
using StudentService.Core.Entities;
using System.Collections.Generic;

namespace StudentService.Core.Interfaces
{
    /// <summary>
    /// Сервисный объект для взаимодействия с данными студентов.
    /// </summary>
    public interface IStudentsService
    {
        /// <summary>
        /// Найти студента по имени и фамилии.
        /// </summary>
        /// <param name="firstName">Имя.</param>
        /// <param name="lastName">Фамилия.</param>
        /// <returns>Экземпляр класса <see cref="StudentDto"/>.</returns>
        StudentDto Get(string firstName, string lastName);

        /// <summary>
        /// Найти студента по адресу электронной почты.
        /// </summary>
        /// <param name="email">Адрес электронной почты.</param>
        /// <returns>Экземпляр класса <see cref="StudentDto"/>.</returns>
        StudentDto Get(string email);

        /// <summary>
        /// Найти студента по Id.
        /// </summary>
        /// <param name="id">Идентификатор студента.</param>
        /// <returns>Экземпляр класса <see cref="StudentDto"/>.</returns>
        StudentDto Get(int id);

        /// <summary>
        /// Найти всех студентов с id из списка.
        /// </summary>
        /// <param name="range">Список идентификаторов студентов.</param>
        /// <returns>Список студентов.</returns>
        List<StudentDto> Get(List<int> range);

        /// <summary>
        /// Найти всех студентов.
        /// </summary>
        /// <param name="pageSize">Количество строк для получения.</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <returns>Список студентов.</returns>
        List<StudentDto> GetAll(int pageSize, int pageNumber);

        /// <summary>
        /// Найти всех студентов.
        /// </summary>
        /// <returns>Список студентов.</returns>
        List<StudentDto> GetAll();

        /// <summary>
        /// Зарегистрировать студента.
        /// </summary>
        /// <param name="student">Данные регистрируемого студента.</param>
        /// <returns>Признак успешной регистрации студента.</returns>
        bool CreateOne(StudentDto student);

        /// <summary>
        /// Зарегистрировать студентов.
        /// </summary>
        /// <param name="students">Список студентов для регистрации.</param>
        void CreateRange(List<StudentDto> students);

        /// <summary>
        /// Обновить данные студента.
        /// </summary>
        /// <param name="student">Данные студента.</param>
        void UpdateOne(StudentDto student);

        /// <summary>
        /// Удалить данные студента.
        /// </summary>
        /// <param name="id">Идентификатор студента.</param>
        void DeleteOne(int id);
    }
}