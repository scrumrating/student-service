﻿using StudentService.Core.Entities;
using System.Collections.Generic;

namespace StudentService.Core.Interfaces
{
    /// <summary>
    /// Интерфейс репозитория с данными студентов.
    /// </summary>
    public interface IStudentsRepository
    {
        /// <summary>
        /// Найти студента по имени и фамилии.
        /// </summary>
        /// <param name="firstName">Имя.</param>
        /// <param name="lastName">Фамилия.</param>
        /// <returns>Экземпляр класса <see cref="Student"/>.</returns>
        Student Find(string firstName, string lastName);

        /// <summary>
        /// Найти студента по адресу электронной почты.
        /// </summary>
        /// <param name="email">Адрес электронной почты.</param>
        /// <returns>Экземпляр класса <see cref="Student"/>.</returns>
        Student Find(string email);

        /// <summary>
        /// Найти студента по Id.
        /// </summary>
        /// <param name="id">Идентификатор студента.</param>
        /// <returns>Экземпляр класса <see cref="Student"/>.</returns>
        Student Find(int id);

        /// <summary>
        /// Найти всех студентов с id из списка.
        /// </summary>
        /// <param name="range">Список идентификаторов студентов.</param>
        /// <returns>Список студентов.</returns>
        List<Student> Find(List<int> range);

        /// <summary>
        /// Найти всех студентов.
        /// </summary>
        /// <param name="pageSize">Количество строк для получения.</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <returns>Список студентов.</returns>
        List<Student> FindAll(int pageSize, int pageNumber);

        /// <summary>
        /// Найти всех студентов.
        /// </summary>
        /// <returns>Список студентов.</returns>
        List<Student> FindAll();

        /// <summary>
        /// Зарегистрировать студента.
        /// </summary>
        /// <param name="student">Данные студента.</param>
        /// <returns>Признак успешной регистрации студента.</returns>
        bool Register(Student student);

        /// <summary>
        /// Зарегистрировать студентов.
        /// </summary>
        /// <param name="students">Список студентов для регистрации.</param>
        void RegisterRange(List<Student> students);

        /// <summary>
        /// Обновить данные студента.
        /// </summary>
        /// <param name="student">Данные студента.</param>
        void Update(Student student);

        /// <summary>
        /// Удалить данные студента.
        /// </summary>
        /// <param name="id">Идентификатор студента.</param>
        void Delete(int id);
    }
}